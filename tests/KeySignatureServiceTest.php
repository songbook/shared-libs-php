<?php declare(strict_types=1);

namespace Test\Songbook\Shared;

use PHPUnit\Framework\TestCase;
use Songbook\Shared\KeySignature\KeySignatureDoesNotExistException;
use Songbook\Shared\KeySignature\KeySignatureService;

class KeySignatureServiceTest extends TestCase
{
    private KeySignatureService $keySignatureService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->keySignatureService = new KeySignatureService();
    }

    public function test_throwExceptionWhenKeySignatureDoesNotExist()
    {
        $this->expectException(KeySignatureDoesNotExistException::class);
        $this->expectExceptionMessage('Key signature B# does not exist');
        $this->keySignatureService->getKeySignature('B#');
    }

    /**
     * @dataProvider getKeySignatures
     */
    public function test_getCircleOfFifthsValue(string $keySignatureName, int $keySignature)
    {
        $this->assertEquals($keySignature, $this->keySignatureService->getKeySignature($keySignatureName));
    }

    public function getKeySignatures(): array
    {
        return [
            ['Cb', 0],
            ['Gb', 1],
            ['Db', 2],
            ['Ab', 3],
            ['Eb', 4],
            ['Bb', 5],
            ['F', 6],
            ['C', 7],
            ['G', 8],
            ['D', 9],
            ['A', 10],
            ['E', 11],
            ['B', 12],
            ['F#', 13],
            ['C#', 14]
        ];
    }
}

