<?php declare(strict_types=1);

namespace Songbook\Shared;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SongbookSharedBundle extends Bundle
{
}