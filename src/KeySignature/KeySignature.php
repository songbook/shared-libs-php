<?php declare(strict_types=1);

namespace Songbook\Shared\KeySignature;

use ValueError;

enum KeySignature: int
{
    case C_FLAT = 0;
    case G_FLAT = 1;
    case D_FLAT = 2;
    case A_FLAT = 3;
    case E_FLAT = 4;
    case B_FLAT = 5;
    case F = 6;
    case C = 7;
    case G = 8;
    case D = 9;
    case A = 10;
    case E = 11;
    case B = 12;
    case F_SHARP = 13;
    case C_SHARP = 14;

    public static function fromName(string $name): KeySignature
    {
        return match ($name) {
            'Cb' => KeySignature::C_FLAT,
            'Gb' => KeySignature::G_FLAT,
            'Db' => KeySignature::D_FLAT,
            'Ab' => KeySignature::A_FLAT,
            'Eb' => KeySignature::E_FLAT,
            'Bb' => KeySignature::B_FLAT,
            'F' => KeySignature::F,
            'C' => KeySignature::C,
            'G' => KeySignature::G,
            'D' => KeySignature::D,
            'A' => KeySignature::A,
            'E' => KeySignature::E,
            'B' => KeySignature::B,
            'F#' => KeySignature::F_SHARP,
            'C#' => KeySignature::C_SHARP,
            default => throw new ValueError("{$name} is not a valid key signature"),
        };
    }

    public function getHumanReadable(): string
    {
        return match ($this) {
            KeySignature::C_FLAT => 'Cb',
            KeySignature::G_FLAT => 'Gb',
            KeySignature::D_FLAT => 'Db',
            KeySignature::A_FLAT => 'Ab',
            KeySignature::E_FLAT => 'Eb',
            KeySignature::B_FLAT => 'Bb',
            KeySignature::F => 'F',
            KeySignature::C => 'C',
            KeySignature::G => 'G',
            KeySignature::D => 'D',
            KeySignature::A => 'A',
            KeySignature::E => 'E',
            KeySignature::B => 'B',
            KeySignature::F_SHARP => 'F#',
            KeySignature::C_SHARP => 'C#'
        };
    }
}

